output "alb_hostname" {
  value = "${aws_alb.main.dns_name}"
}

output "ecr-repository-url" {
  value = "${aws_ecr_repository.docker_image.repository_url}"
}

output "ecr-repository-name" {
  value = "${aws_ecr_repository.docker_image.name}"
}